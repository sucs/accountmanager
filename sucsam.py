debugmode = 0

import os
import pwd
import sys
import readline
import psycopg2
import datetime
import codecs
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import ldap
import subprocess


def correctPaidUntilStr():
	# same logic as suapi stuff
	# if it's greater than Sept then year+1
	# if it's less than Spet then year
	if month >= 9:
		paidyear = year + 1
	elif month < 8:
		paidyear = year
	else:
		print("Something went really wrong")
		sys.exit(3)
	return "Sept. " + str(paidyear)


# annoyingly when people have paid until is stored as a string in the DB
# as something like "Sept. $YEAR"
# so explode the str on " " cast $year to int and compare to correct year
def stripFuturePeople(listofpeeps):
	badpeeps = []
	for person in listofpeeps:
		if person[1] == "delete":
			badpeeps.append(person)
		else:
			paidyeararray = person[1].split(' ')
			if month >= 9:
				paidyear = year + 1
			elif month < 8:
				paidyear = year
			else:
				print("Something went really wrong")
				sys.exit(3)
			if int(paidyeararray[1]) < paidyear:
				badpeeps.append(person)
	return badpeeps


# ok this needs explaning
# in comes a list [('user1', 'Sept. 2015'), ('user2', 'Sept. 2014')]
# we need to work out what account type it is,
# select type from members where username=$peep[0]
# then from that find the right email text file and send
# easy peasy
def sendReminderEmail(listofpeeps):
	# if somenes been marked as delete, don't remind them to renew
	for person in listofpeeps:
		if person[1] == "delete":
			listofpeeps.remove(person)

	for person in listofpeeps:
		cur = DBconn.cursor()	
		cur.execute("SELECT * from members WHERE username=%(user)s",{"user" : person[0]})
		DBdata = cur.fetchall()
		cur.close()

		username = DBdata[0][1]
		realname = DBdata[0][2]
		email = DBdata[0][3]
		acctype = DBdata[0][7]
		adminname = pwd.getpwuid(os.geteuid())[0]

		if os.path.isfile(str(acctype) + "-email"):
			file = codecs.open(str(acctype) + "-email", encoding='utf-8')
			data = file.read()
			file.close()
			data = data.replace("{$realname}",realname)
			data = data.replace("{$username}",username)
			data = data.replace("{$adminname}",adminname)
			sender = 'staff@sucs.org'
			if debugmode > 0:
				receiver = adminname + '@sucs.org'
			else:
				receiver = email
			message = MIMEMultipart()
			message['From'] = sender
			message['To'] = receiver
			message['Subject'] = 'Your SUCS account is due for renewal'
			messageBody = MIMEText(data, 'plain', "UTF-8")
			message.attach(messageBody)

			smtpConn = smtplib.SMTP('localhost')
			smtpConn.sendmail(sender, receiver, message.as_string())
			smtpConn.quit()

			print("Sent reminder email to " + username + " on " + str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))

			if debugmode > 0:  # only send out one email instead of 85464684164864165
				sys.exit(0)
		else:
			print("Renewal email template not found!")
			sys.exit(4)
	if debugmode == 0:
			file = codecs.open("renewal-notify-email", encoding='utf-8')
			data = file.read()
			file.close()
			data = data.replace("{$adminname}",adminname)
			data = data.replace("{$peoples}",str(listofpeeps))
			sender = "staff@sucs.org"
			receiver = "exec@sucs.org"
			cc = "logs@sucs.org"
			message = MIMEMultipart()
			message['From'] = sender
			message['To'] = receiver
			message['Cc'] = cc
			message['Subject'] = 'Renewal Reminder Sent Out'
			messageBody = MIMEText(data, 'plain', "UTF-8")
			message.attach(messageBody)

			smtpConn = smtplib.SMTP('localhost')
			smtpConn.sendmail(sender, receiver + "," + cc, message.as_string())
			smtpConn.quit()


def mainMenu():
	print("\nPlease choose an option")
	print("[h] Diplay some help")
	print("[qs] View quick stats of all accounts")
	print("[la] View a list of ALL accounts")
	print("[r] Renewals & Reminders")
	print("[d] Delete accounts")
	print("[q] Quit")

	option = input("Option: ")
	print("\n")
	
	if option == "h":
		print("help text comming soon")
		mainMenu()
	elif option == "q":
		DBconn.close()
		sys.exit(0)
	elif option == "qs":
		quickStats()
		mainMenu()
	elif option == "la":
		listUsers()
		mainMenu()
	elif option == "r":
		reminderMenu()
		mainMenu()
	elif option == "d":
		deleteMenuInit()
		mainMenu()
	else:
		mainMenu()


def quickStats():
	print("There are:")
	print(str(len(students))+" Students")
	paidyears = {}
	for item in students:
		paidyears[item[1]] = paidyears.get(item[1], 0) + 1
	for item in sorted(paidyears):
		print(" - " + str(paidyears[item]) + ": " + str(item))
	print(str(len(societies))+" Societies")
	paidyears = {}
	for item in societies:
		paidyears[item[1]] = paidyears.get(item[1], 0) + 1
	for item in sorted(paidyears):
		print(" - " + str(paidyears[item]) + ": " + str(item))
	print(str(len(associates))+" Associates")
	paidyears = {}
	for item in associates:
		paidyears[item[1]] = paidyears.get(item[1], 0) + 1
	for item in sorted(paidyears):
		print(" - " + str(paidyears[item]) + ": " + str(item))
	print(str(len(lifers))+" Lifers")
	print(str(len(hons))+" Honoary")


def listUsers():
	print("Students: ")
	for student in students:
		print(str(student[0]) + " (" + str(student[1]) + ")")
	print("\nSocieties: ")
	for soc in societies:
		print(str(soc[0]) + " (" + str(soc[1]) + ")")
	print("\nAssociates: ")
	for associate in associates:
		print(str(associate[0]) + " (" + str(associate[1]) + ")")
	print("\nLifers: ")
	for life in lifers:
		print(str(life[0]) + " (" + str(life[1]) + ")")
	print("\nHonoary: ")
	for hon in hons:
		print(str(hon[0]) + " (" + str(hon[1]) + ")")


def emailDeletedUser(userDBinfo):
	username = userDBinfo[0][1]
	realname = userDBinfo[0][2]
	email = userDBinfo[0][3]
	adminname = pwd.getpwuid(os.geteuid())[0]

	file = codecs.open("deleted-email", encoding='utf-8')
	data = file.read()
	file.close()
	data = data.replace("{$realname}",realname)
	data = data.replace("{$username}",username)
	data = data.replace("{$adminname}",adminname)
	sender = 'staff@sucs.org'
	receiver = email
	message = MIMEMultipart()
	message['From'] = sender
	message['To'] = receiver
	message['Subject'] = 'Your SUCS account has been deleted'
	messageBody = MIMEText(data, 'plain', "UTF-8")
	message.attach(messageBody)

	smtpConn = smtplib.SMTP('localhost')
	smtpConn.sendmail(sender, receiver, message.as_string())
	smtpConn.quit()


def emailAdminDeleteUser(username):
	adminname = pwd.getpwuid(os.geteuid())[0]

	file = codecs.open("deleted-notify-email", encoding='utf-8')
	data = file.read()
	file.close()
	data = data.replace("{$username}",username)
	data = data.replace("{$adminname}",adminname)
	sender = 'staff@sucs.org'
	receiver = "logs@sucs.org"
	message = MIMEMultipart()
	message['From'] = sender
	message['To'] = receiver
	message['Subject'] = username + '\'s SUCS account has been deleted'
	messageBody = MIMEText(data, 'plain', "UTF-8")
	message.attach(messageBody)

	smtpConn = smtplib.SMTP('localhost')
	smtpConn.sendmail(sender, receiver, message.as_string())
	smtpConn.quit()


def deleteUser(peopleList):

	# get fs ready
	basedir = "/home/deleted"
	mboxdir = "/var/mail/"
	subprocess.call(['sudo', 'mkdir', "-m", "700", basedir])

	for person in peopleList:

		# get all their info from the db
		cur = DBconn.cursor()
		cur.execute("SELECT * from members WHERE username=%(user)s",{"user" : person})
		userDBinfo = cur.fetchall()
		cur.close()

		# fail safe, if the DB username we just got isn't the one we are
		# messing with, die
		if str(person) != userDBinfo[0][1]:
			print("Something went wrong getting info from DB, quitting! NO CHNAGES MADE")
			sys.exit(9)

		# get all their info from the ldap
		userLDAPinfo = ldapconn.search_s(ldap_base,ldap.SCOPE_SUBTREE,"uid="+str(person))

		# fail safe, if the ldap username we just got isn't the one we
		# are messing with, die
		if str(person) != userLDAPinfo[0][1]["uid"][0]:
			print("Something went wrong getting info from LDAP, quitting! NO CHNAGES MADE")
			sys.exit(9)

		# declare some easy to use vars
		username = str(person)
		persondir = basedir+"/"+username

		# make the dir to store their stuff
		subprocess.call(["sudo", "mkdir", persondir])

		# kill all their procs
		subprocess.call(["sudo", "pkill", "-u", username])

		# backup their ldap entry
		# sudo ldapsearch -x -D "cn=Manager,dc=sucs,dc=org" -y /etc/ldap.secret  "(uid=imranh)"
		ldapbackupfile = open("./ldap-ldif", "w")
		subprocess.call(["sudo", "ldapsearch", "-x", "-D", ldap_manager, "-y", ldap_manager_pass, "-L", "(uid="+username+")"], stdout=ldapbackupfile)
		ldapbackupfile.close()
		# sanity check we have a legit backup
		if "# numEntries: 1" not in open("./ldap-ldif").read():
			print("Something went wrong getting a backup LDAP entry, quitting! NO CHNAGES MADE")
			sys.exit(9)
		else:
			print("LDAP Backup made!")
		subprocess.call(['sudo', 'mv', "-f", "./ldap-ldif", persondir+"/ldap-ldif"])

		# delete them from ldap
		# ldapconn.delete_s(ldap_base,ldap.SCOPE_SUBTREE,"uid="+str(person[0]))
		# sudo ldapdelete -D "cn=Manager,dc=sucs,dc=org" -y /etc/ldap.secret "uid=imran,ou=people,dc=sucs,dc=org"
		rc = subprocess.call(["sudo", "ldapdelete", "-D", ldap_manager, "-y", ldap_manager_pass, userLDAPinfo[0][0]])

		# sanity check ldap delete happened
		if int(rc) != 0:
			print("Something went wrong deleting the LDAP entry, quitting! NO CHNAGES MADE")
			sys.exit(9)

		# delete their useradd ldif file
		subprocess.call(['sudo', 'rm', "-v", "/home/useradd/useradd." + username + ".ldif"])

		# move their homedir to the deleted folder
		subprocess.call(['sudo', 'mv', "-f", userLDAPinfo[0][1]["homeDirectory"][0], persondir+"/homedir"])

		# move their mail to the deleted folder
		subprocess.call(['sudo', 'mv', "-f", mboxdir+username, persondir+"/mbox"])

		# if they have a apcahe vhost kill it
		if os.path.exists("/etc/apache2/sites-enabled/" + username + ".conf"):
			subprocess.call(['sudo', 'a2dissite', username + ".conf"])
			subprocess.call(['sudo', 'systemctl', "reload", "apache2.service"])

		# remove them from the printer
		# pkusers --delete imranh
		subprocess.call(['sudo', 'pkusers', "--delete", username])

		# remove from mailing lists
		# /usr/lib/mailman/bin/remove_members members imranh@sucs.org
		# /usr/lib/mailman/bin/remove_members users imranh@sucs.org
		subprocess.call(['sudo', '/usr/lib/mailman/bin/remove_members', "members", userLDAPinfo[0][1]["mail"][0]])  # sucs email
		subprocess.call(['sudo', '/usr/lib/mailman/bin/remove_members', "members", userDBinfo[0][3]])  # main email
		subprocess.call(['sudo', '/usr/lib/mailman/bin/remove_members', "members", str(userDBinfo[0][6])+"@swan.ac.uk"])  # iss email
		subprocess.call(['sudo', '/usr/lib/mailman/bin/remove_members', "users", userLDAPinfo[0][1]["mail"][0]])  # sucs email
		subprocess.call(['sudo', '/usr/lib/mailman/bin/remove_members', "users", userDBinfo[0][3]])  # main email
		subprocess.call(['sudo', '/usr/lib/mailman/bin/remove_members', "users", str(userDBinfo[0][6])+"@swan.ac.uk"])  # iss email

		# time to delete them from the db :(
		cur = DBconn.cursor()
		# delete from doorcards first
		cur.execute("DELETE FROM doorcards WHERE uid=%(uid)s",{"uid" : str(userDBinfo[0][0])})
		# delete from printer table
		cur.execute("DELETE FROM printer WHERE username=%(user)s",{"user" : username})
		# delete from members table
		cur.execute("DELETE FROM members WHERE username=%(user)s",{"user" : username})
		# add their some details to the oldmembers table
		cur.execute("INSERT into oldmembers (username,realname) VALUES (%(user)s,%(realname)s)",{"user" : username,"realname" : userDBinfo[0][2]})
		DBconn.commit()
		cur.close()

		# call the script to update door
		# /usr/local/bin/db-to-cards.sh
		subprocess.call(['/usr/local/bin/db-to-cards.sh'])

		# notify people
		# us
		print(username + " was successfully deleted on " + str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
		# them
		emailDeletedUser(userDBinfo)
		# logs@
		emailAdminDeleteUser(person)
		
		# remove the entry from the list
		#peopleList.remove(person)
		
	# return to the deletemenu with an empty list as we've just deleted everyone
	#deleteMenu(peopleList)
	deleteMenuInit()


def reminderMenu():
	print("")
	print("Double check https://sucs.org/Admin/SU that everyone that has paid to renew has been marked as renewed. This tool doesn't do that job yet!")
	print("")
	print("1. View a list of accounts that aren't marked as renewed in the DB")
	print("2 Send email to ALL reminding them to renew")
	print("2.1 Send email to all STUDENTS reminding them to renew")
	print("2.2 Send email to all SOCIETIES reminding them to renew")
	print("2.3 Send email to all ASSOCIATES reminding them to renew")
	print("0. Return to main menu")

	option = input("Option: ")

	if option == "1":
		for student in studentsBad:
			print(str(student[0]) + " last paid: " + str(student[1]))
		for soc in societiesBad:
			print(str(soc[0]) + " last paid: " + str(soc[1]))
		print("Not emailing associates as we don't have a programatic way of checking their status.")
		#for ass in associatesBad:
		#	print(str(ass[0]) + " last paid: " + str(ass[1]))
		reminderMenu()
	if option == "2":
		if len(studentsBad) > 0:
			sendReminderEmail(studentsBad)
		if len(societiesBad) > 0:
			sendReminderEmail(societiesBad)
		if len(associatesBad) > 0:
			sendReminderEmail(associatesBad)
	if option == "2.1":
		if len(studentsBad) > 0:
			sendReminderEmail(studentsBad)
	if option == "2.2":
		if len(societiesBad) > 0:
			sendReminderEmail(societiesBad)
	if option == "2.3":
		print("Not emailing associates as we don't have a programatic way of checking their status.")
		#if len(associatesBad) > 0:
		#	sendReminderEmail(associatesBad)
	if option == "0":
		mainMenu()
	else:
		reminderMenu()


def deleteMenu(deleteArray):
	print("")
	print("1. View list of accounts to be deleted")
	print("2. Add/delete users from delete list")
	print("3. Auto populate list of users to be deleted")
	print("3.1 Just add student accounts")
	print("3.2 Just add student accounts from a specific year")
	print("3.5 Just add accounts marked as 'delete' in the DB")
	print("9. Do the delete")
	print("0. Go back to main menu")
	
	option = input("Option: ")

	if option == "1":
		print("\nThe following users will be deleted: ")
		for user in deleteArray:
			print(user)
		deleteMenu(deleteArray)
	elif option == "2":
		print("Enter a username to add or remoeve it from the list.")
		user = input("Username: ")
		if user in deleteArray:
			deleteArray.remove(user)
			print(str(user) + " won't be deleted.")
		else:
			try:
				pwd.getpwnam(user)
				deleteArray.append(user)
				print(str(user) + " will be deleted.")
			except KeyError:
				print("\x1b[1;31mInvalid user.\x1b[0m")
		deleteMenu(deleteArray)
	elif option == "3":
		if len(studentsBad) > 0:
			for student in studentsBad:
				print("Adding " + str(student[0]) + " to the list because the DB says: " + str(student[1]))
				deleteArray.append(student[0])
		if len(societiesBad) > 0:
			for soc in societiesBad:
				print("Adding " + str(soc[0]) + " to the list because the DB says: " + str(soc[1]))
				deleteArray.append(soc[0])
		if len(associatesBad) > 0:
			for ass in associatesBad:
				print("Adding " + str(ass[0]) + " to the list because the DB says: " + str(ass[1]))
				deleteArray.append(ass[0])
		deleteMenu(deleteArray)
	elif option == "3.1":
		if len(studentsBad) > 0:
			for student in studentsBad:
				print("Adding " + str(student[0]) + " to the list because the DB says: " + str(student[1]))
				deleteArray.append(student[0])
		print("No students look like the need deleting.")
		deleteMenu(deleteArray)
	elif option == "3.2":
		year = input("Please enter the year you wish to target: ")
		if len(studentsBad) > 0:
			targetedPeople = []
			for student in studentsBad:
				paidyeararray = student[1].split(' ')
				if year == paidyeararray[1]:
					targetedPeople.append(student[0])
					print("Adding " + student[0] + " to the list because the DB says: " + str(student[1]))
			deleteMenu(targetedPeople)
	elif option == "3.5":
		if len(studentsBad) > 0:
			for student in studentsBad:
				if student[1] == "delete":
					print("Adding " + str(student[0]) + " to the list because the DB says: " + str(student[1]))
					deleteArray.append(student[0])
		if len(societiesBad) > 0:
			for soc in societiesBad:
				if soc[1] == "delete":
					print("Adding " + str(soc[0]) + " to the list because the DB says: " + str(soc[1]))
					deleteArray.append(soc[0])
		if len(associatesBad) > 0:
			for ass in associatesBad:
				if ass[1] == "delete":
					print("Adding " + str(ass[0]) + " to the list because the DB says: " + str(ass[1]))
					deleteArray.append(ass[0])
		deleteMenu(deleteArray)
	elif option == "9":
		if debugmode > 0:
			print("Debugmode is set. Doing No Deletes.")
			deleteMenu(deleteArray)
		elif debugmode == 0:
			deleteUser(deleteArray)
	elif option == "0":
		mainMenu()
	else:
		deleteMenu(deleteArray)


def deleteMenuInit():
	# list that store usernames to be deleted
	# gets reset everytime you go into this bit on purpose
	tobedeleted = []

	deleteMenu(tobedeleted)


### MAIN ###


if os.geteuid() == 0:
	print("\x1b[1;31mDon't run this as root!\x1b[0m")
	sys.exit(1)

# get some date/time info ready
now = datetime.datetime.now()
year = now.year
month = now.month

# This tool was written for python *3*
# but doesn't mean we can't support 2 as well :)
try:
	input = raw_input
except NameError:
	pass

# try and connect to the db
try:
	DBconn = psycopg2.connect(database="sucs")
except:
	print("Can't connect to the SUCS DB, suiciding!")
	sys.exit(2)

# try and connect to ldap
try:
	ldapconn = ldap.initialize("ldap://sucs.org")
	ldapconn.simple_bind_s("","")
	ldap_base = "ou=People,dc=sucs,dc=org"
	ldap_manager = "cn=Manager,dc=sucs,dc=org"
	ldap_manager_pass = "/etc/ldap.secret"
except:
	print("Can't connect to the SUCS LDAP, suiciding")
	sys.exit(2)

# store some data from to db to operate on
cur = DBconn.cursor()

cur.execute("SELECT username,paid from members WHERE type=1 ORDER BY paid")
students = cur.fetchall()
studentsBad = stripFuturePeople(students)
cur.execute("SELECT username,paid from members WHERE type=2 ORDER BY paid")
societies = cur.fetchall()
societiesBad = stripFuturePeople(societies)
cur.execute("SELECT username,paid from members WHERE type=5 ORDER BY paid")
associates = cur.fetchall()
associatesBad = stripFuturePeople(associates)
cur.execute("SELECT username,paid from members WHERE type=4 ORDER BY username")
lifers = cur.fetchall()
cur.execute("SELECT username,paid from members WHERE type=3 ORDER BY username")
hons = cur.fetchall()

cur.close()

print("\x1b[33mWelcome to the SUCS Account Manager!\x1b[0m")
mainMenu()
